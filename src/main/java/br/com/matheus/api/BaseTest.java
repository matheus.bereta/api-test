package br.com.matheus.api;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import static io.restassured.RestAssured.*;

@Listeners(ExtentITestListenerClassAdapter.class)
public class BaseTest {

    public static RequestSpecification spec;

    @BeforeClass
    public void setUp() {
        baseURI = "https://jsonplaceholder.typicode.com/";
        spec = new RequestSpecBuilder()
                .setRelaxedHTTPSValidation()
                .setContentType(ContentType.JSON)
                .build();

        enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
