package br.com.matheus.api.functional;

import br.com.matheus.api.BaseTest;
import br.com.matheus.api.objects.Post;
import br.com.matheus.api.objects.Response;
import org.testng.annotations.Test;
import br.com.matheus.api.datadriven.GetPostsDataProvider;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class FunctionalPostsGetTest extends BaseTest {


    @Test(dataProvider = "getPosts", dataProviderClass = GetPostsDataProvider.class)
    public void validateGetPosts(Post post) {
        given().
            spec(spec).
            pathParam("id", post.getId()).
        when().
            get("posts/{id}").
        then().
            statusCode(200).
            body("userId", in(Response.getUserIds()),
                    "title", notNullValue(),
                    "body", notNullValue(),
                    "id", is(post.getId()));
    }

    @Test
    public void validateGetPostsNotFound() {
        given().
            spec(spec).
            pathParam("id", 200).
        when().
            get("posts/{id}").
        then().
            statusCode(404);
    }
}
