package br.com.matheus.api.functional;

import br.com.matheus.api.BaseTest;
import br.com.matheus.api.objects.Post;
import br.com.matheus.api.datadriven.PostPostsDataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

public class FunctionalPostsPostTest extends BaseTest {

    @Test(dataProvider = "postPosts", dataProviderClass = PostPostsDataProvider.class)
    public void validatePostPosts(Post post) {
        given().
            spec(spec).
            body(post).
        when().
            post("posts").
        then().
            statusCode(201).
            body("title", is(post.getTitle()),
                    "body", is(post.getBody()),
                    "userId", is(post.getUserId()),
                    "id", greaterThan(100));
    }
}
