package br.com.matheus.api.datadriven;

import br.com.matheus.api.objects.Post;
import com.github.javafaker.Faker;
import lombok.experimental.UtilityClass;
import org.testng.annotations.DataProvider;

@UtilityClass
public class GetPostsDataProvider {

    Faker faker = new Faker();

    @DataProvider(name = "getPosts")
    public static Object[][] getPosts() {
        Post post1 = Post.builder()
                .id(faker.number().numberBetween(1, 10))
                .build();

        Post post2 = Post.builder()
                .id(faker.number().numberBetween(11, 20))
                .build();

        Post post3 = Post.builder()
                .id(faker.number().numberBetween(21, 30))
                .build();

        Post post4 = Post.builder()
                .id(faker.number().numberBetween(31, 40))
                .build();

        Post post5 = Post.builder()
                .id(faker.number().numberBetween(41, 50))
                .build();

        Post post6 = Post.builder()
                .id(faker.number().numberBetween(51, 60))
                .build();

        Post post7 = Post.builder()
                .id(faker.number().numberBetween(61, 70))
                .build();

        Post post8 = Post.builder()
                .id(faker.number().numberBetween(71, 80))
                .build();

        Post post9 = Post.builder()
                .id(faker.number().numberBetween(81, 90))
                .build();

        Post post10 = Post.builder()
                .id(faker.number().numberBetween(91, 100))
                .build();

        return new Object[][]{
                {post1}, {post2}, {post3}, {post4}, {post5}, {post6}, {post7}, {post8}, {post9}, {post10}
        };
    }
}
