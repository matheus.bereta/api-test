package br.com.matheus.api.healthcheck;

import br.com.matheus.api.BaseTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class HealthCheckTest extends BaseTest {

    @Test
    public void healthCheck() {
        given().
            spec(spec).
        when().
            get("posts/").
        then().
            statusCode(200);
    }
}
