package br.com.matheus.api.datadriven;

import br.com.matheus.api.objects.Post;
import com.github.javafaker.Faker;
import lombok.experimental.UtilityClass;
import org.testng.annotations.DataProvider;

@UtilityClass
public class PutPostsDataProvider {

    Faker faker = new Faker();

    @DataProvider(name = "putPosts")
    public static Object[][] putPosts() {
        Post post1 = Post.builder()
                .id(faker.number().numberBetween(1, 10))
                .title(faker.app().name())
                .build();

        Post post2 = Post.builder()
                .id(faker.number().numberBetween(11, 20))
                .body(faker.avatar().image())
                .build();

        Post post3 = Post.builder()
                .id(faker.number().numberBetween(21, 30))
                .userId(faker.number().randomDigit())
                .build();

        Post post4 = Post.builder()
                .id(faker.number().numberBetween(31, 40))
                .title(faker.aviation().airport())
                .userId(faker.number().randomDigit())
                .build();

        Post post5 = Post.builder()
                .id(faker.number().numberBetween(41, 50))
                .body(faker.dog().name())
                .userId(faker.number().randomDigit())
                .build();

        Post post6 = Post.builder()
                .id(faker.number().numberBetween(51, 60))
                .body(faker.finance().creditCard())
                .title(faker.finance().iban())
                .build();

        Post post7 = Post.builder()
                .id(faker.number().numberBetween(61, 70))
                .title(faker.howIMetYourMother().catchPhrase())
                .build();

        Post post8 = Post.builder()
                .id(faker.number().numberBetween(71, 80))
                .body(faker.university().name())
                .build();

        Post post9 = Post.builder()
                .id(faker.number().numberBetween(81, 90))
                .userId(faker.number().randomDigit())
                .build();

        Post post10 = Post.builder()
                .id(faker.number().numberBetween(91, 100))
                .title(faker.programmingLanguage().name())
                .body(faker.programmingLanguage().creator())
                .userId(faker.number().randomDigit())
                .build();

        return new Object[][]{
                {post1}, {post2}, {post3}, {post4}, {post5}, {post6}, {post7}, {post8}, {post9}, {post10}
        };
    }
}
