package br.com.matheus.api.datadriven;

import br.com.matheus.api.objects.Post;
import com.github.javafaker.Faker;
import lombok.experimental.UtilityClass;
import org.testng.annotations.DataProvider;

@UtilityClass
public class PostPostsDataProvider {

    Faker faker = new Faker();

    @DataProvider(name = "postPosts")
    public static Object[][] postPosts() {
        Post post1 = Post.builder()
                .title(faker.rockBand().name())
                .body(faker.artist().name())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post2 = Post.builder()
                .title(faker.country().name())
                .body(faker.country().capital())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post3 = Post.builder()
                .title(faker.animal().name())
                .body(faker.cat().name())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post4 = Post.builder()
                .title(faker.book().title())
                .body(faker.book().author())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post5 = Post.builder()
                .title(faker.color().name())
                .body(faker.color().hex())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post6 = Post.builder()
                .title(faker.gameOfThrones().house())
                .body(faker.gameOfThrones().city())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post7 = Post.builder()
                .title(faker.harryPotter().house())
                .body(faker.harryPotter().character())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post8 = Post.builder()
                .title(faker.esports().team())
                .body(faker.esports().player())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post9 = Post.builder()
                .title(faker.company().name())
                .body(faker.company().profession())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        Post post10 = Post.builder()
                .title(faker.food().sushi())
                .body(faker.food().ingredient())
                .userId(faker.number().numberBetween(1, 10))
                .build();

        return new Object[][]{
                {post1}, {post2}, {post3}, {post4}, {post5}, {post6}, {post7}, {post8}, {post9}, {post10}
        };
    }
}
