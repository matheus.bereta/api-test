package br.com.matheus.api.contract;

import br.com.matheus.api.BaseTest;
import org.testng.annotations.Test;

import java.io.File;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class ContractPostsTest extends BaseTest {

    @Test
    public void validateContractGetPosts() {
        given().
            spec(spec).
        when().
            get("posts/").
        then().
            body(matchesJsonSchema(
                    new File("src/test/resources/json_schemas/posts/posts_schema.json")));
    }
}
