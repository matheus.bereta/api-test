package br.com.matheus.api.functional;

import br.com.matheus.api.BaseTest;
import br.com.matheus.api.objects.Post;
import br.com.matheus.api.datadriven.PutPostsDataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class FunctionalPostsPutTest extends BaseTest {

    @Test(dataProvider = "putPosts", dataProviderClass = PutPostsDataProvider.class)
    public void validatePutPosts(Post post) {
        given().
            spec(spec).
            pathParam("id", post.getId()).
            body(post).
        when().
            put("posts/{id}").
        then().
            statusCode(200).
            body("title", is(post.getTitle()),
                    "body", is(post.getBody()),
                    "userId", is(post.getUserId()),
                    "id", is(post.getId()));
    }
}
