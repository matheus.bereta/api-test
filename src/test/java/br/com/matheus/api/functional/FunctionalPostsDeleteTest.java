package br.com.matheus.api.functional;

import br.com.matheus.api.BaseTest;
import br.com.matheus.api.objects.Post;
import br.com.matheus.api.datadriven.DeletePostsDataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class FunctionalPostsDeleteTest extends BaseTest {

    @Test(dataProvider = "deletePosts", dataProviderClass = DeletePostsDataProvider.class)
    public void validateDeletePosts(Post post) {
        given().
            spec(spec).
            pathParam("id", post.getId()).
        when().
            delete("posts/{id}").
        then().
            statusCode(200);
    }
}
